﻿# Adapter Pattern TP

L'exercice consiste à compléter la classe "SquareAdapter" permettant à la classe "Square" d'implémenter l'interface "Shape".  
**Aucun autre fichier ne doit être modifié !**

L'exercice est réussi lorsque l'output corresponds à ceci :

Rectangle with coordinate left-down point (10:20), width: 20, height: 40  
Line from point A(10:20), to point B(30:60)  
Square with coordinate left-down point (10:20), size: 20