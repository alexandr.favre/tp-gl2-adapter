
public class Main {

	// You should have this output :
	// Rectangle with coordinate left-down point (10:20), width: 20, height: 40
	// Line from point A(10:20), to point B(30:60)
	// Square with coordinate left-down point (10:20), size: 20
	public static void main(String[] args) {

		Shape[] shapes = { new RectangleAdapter(new Rectangle()), new Line(),
				new SquareAdapter(new Square()) };
		int x1 = 10, y1 = 20;
		int x2 = 30, y2 = 60;
		for (Shape shape : shapes) {
			shape.draw(x1, y1, x2, y2);
		}
	}
}
